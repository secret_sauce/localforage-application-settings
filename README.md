localForage-application-settings
=================================

A application settings driver for [localForage](https://github.com/mozilla/localForage).

# Requirements

* [localForage](https://github.com/mozilla/localForage) v1.2.1+

# Installation
`npm i localforage-application-settings`
