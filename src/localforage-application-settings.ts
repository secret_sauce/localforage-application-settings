import * as appSettings from 'application-settings';
import * as application from 'application';

let support = application.ios != null || application.android != null;
let storageKeys: string[] = [];
let keyPrefix: string = '';

interface LocalForageOptions {
    driver?: string;

    name?: string;

    size?: number;

    storeName?: string;

    version?: string;

    description?: string;
}

function _initStorage(options: LocalForageOptions): void {
    if (options.name) keyPrefix = options.name += '/';
    if (options.storeName) keyPrefix += options.storeName + '/';

}

function clear(callback: (err: any) => void): void {
    let self = this;
    appSettings.clear();
    keys.length = 0;

    callback(null);
}

function getItem<T>(key: string, callback: (err: any, value: T) => void): void {
    let type: T;
    let val: any;
    let fullKey = keyPrefix + key;

    if (!appSettings.hasKey(fullKey)) {
        return callback(new Error('Key missing'), null);
    }

    switch (typeof (type)) {
        case "string":
            val = appSettings.getString(fullKey);
            break;
        case "number":
            val = appSettings.getNumber(fullKey);
            break;
        case "boolean":
            val = appSettings.getBoolean(fullKey);
            break;

        default:
            val = JSON.parse(appSettings.getString(fullKey));
            break;
    }

    return callback(null, val);
}

function key(keyIndex: number, callback: (err: any, key: string) => void): void {
    var result;
    try {
        result = storageKeys[keyIndex];
    } catch (error) {
        result = null;
    }

    callback(null, result);
}

function keys(callback: (err: any, keys: string[]) => void): void {
    callback(null, storageKeys);
}

function length(callback: (err: any, numberOfKeys: number) => void): void {
    callback(null, keys.length);
}

function removeItem(key: string, callback: (err: any) => void): void {
    let fullkey = keyPrefix + key;

    appSettings.remove(fullkey);
    let index = storageKeys.indexOf(fullkey);
    storageKeys.splice(index, 1);

    callback(null);
}

function setItem(key: string, value: any, callback: (err: any, value: any) => void): void {
    let fullkey = keyPrefix + key;

    switch (typeof (value)) {
        case "string":
            appSettings.setString(fullkey, value);
            break;
        case "number":
            appSettings.setNumber(fullkey, value);
            break;
        case "boolean":
            appSettings.setBoolean(fullkey, value);
            break;

        default:
            appSettings.setString(fullkey, JSON.parse(value));
            break;
    }
    
    storageKeys.push(key);

    callback(null, value)
}

export var ApplicationSettingsWrapper = {
    _driver: 'ApplicationSettingsWrapper',
    _initStorage: _initStorage,
    _support: support,
    clear,
    getItem,
    key,
    keys,
    length,
    removeItem,
    setItem
};